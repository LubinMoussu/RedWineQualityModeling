import pandas
import pandas_profiling as pp
import pandas as pd
import os, sys
from matplotlib import pyplot
import numpy as np

try:
    from sklearn.preprocessing import MinMaxScaler, LabelEncoder, OneHotEncoder, StandardScaler, RobustScaler
    import xgboost as xgb
    from sklearn.model_selection import train_test_split
    from sklearn.metrics import accuracy_score, mean_squared_error, mean_absolute_error
    from sklearn.feature_selection import SelectFromModel
    from sklearn.decomposition import PCA
except OSError:
    pass


class RedWineQualityModeling:
    sep = ";"
    params = {"objective": "reg:squarederror", 'colsample_bytree': 0.3, 'learning_rate': 0.1,
              'max_depth': 5, 'alpha': 10}

    def __init__(self, data_file):
        self.data_file = data_file
        self.df = None
        self.features = None
        self.labels = None
        self.x = None
        self.y = None
        self.data_dmatrix = None
        self.x_train = None
        self.x_test = None
        self.y_train = None
        self.y_test = None
        self.model = None
        self.scaler = None
        self.y_pred = None
        self.label_encoder_classes = None

    def load_dataset(self):
        """
        Read a comma-separated values (csv) file into DataFrame.
        :return:
        """
        self.df = pandas.read_csv(self.data_file, sep=self.sep).replace('"', '', regex=True)

    def generate_profiling_report(self):
        """
        do an exploratory data analysis. Save to file
        :return:
        """
        report = pp.ProfileReport(self.df)
        report.to_file('profile_report.html')

    def split_df_into_x_and_y(self, column_name: str):
        """
        split df into x and y set given the column_name
        :param column_name:
        :return:
        """
        if not isinstance(column_name, str):
            raise TypeError
        self.x = self.df.loc[:, self.df.columns != column_name]
        self.y = self.df.loc[:, column_name]

    def get_features_labels(self):
        """
        get data from self.x and self.y
        :return:
        """
        self.features = self.x.values[:, 1:]
        self.labels = self.y.values

    def remove_columns(self, column_name):
        self.df.drop(column_name, axis=1, inplace=True)

    def initialize_scaler(self, scaler: str):
        """

        :param scaler:
        :return:
        """
        if not isinstance(scaler, str):
            raise TypeError
        if scaler == "MinMaxScaler":
            self.scaler = MinMaxScaler((-1, 1))
        if scaler == "StandardScaler":
            self.scaler = StandardScaler()
        if scaler == "RobustScaler":
            self.scaler = RobustScaler()

    def standardize_features(self):
        """
        Standardize the features
        :return:
        """
        return self.scaler.fit_transform(self.features)

    def split_training_testing_sets(self, test_size=0.2, random_state=7):
        if not isinstance(test_size, float) or not isinstance(random_state, int):
            raise TypeError
        self.x_train, self.x_test, self.y_train, self.y_test = train_test_split(
            self.scaler.fit_transform(self.features),
            self.labels, test_size=test_size,
            random_state=random_state)

    def fit_gradient_boosting_model(self):
        self.model.fit(self.x_train, self.y_train)

    def initiate_classifier(self, classifier: str):
        if not isinstance(classifier, str):
            raise TypeError
        if classifier == "XGBClassifier":
            self.model = xgb.XGBClassifier()

    def initiate_regressor(self):
        self.model = xgb.XGBRegressor(objective='reg:linear', colsample_bytree=0.3, learning_rate=0.1,
                                      max_depth=5, alpha=10, n_estimators=10)

    def generate_y_pred(self):
        self.y_pred = self.model.predict(self.x_test)

    def get_feature_importances(self):
        return self.model.feature_importances_

    def setup_classification_workflow(self):
        self.split_df_into_x_and_y(column_name='quality')
        self.get_features_labels()
        self.initialize_scaler("MinMaxScaler")
        self.split_training_testing_sets()
        self.initiate_classifier("XGBClassifier")
        self.fit_gradient_boosting_model()
        self.generate_y_pred()
        return accuracy_score(self.y_test, self.y_pred)

    def setup_regression_workflow(self):
        self.split_df_into_x_and_y(column_name='quality')
        self.get_features_labels()
        self.initialize_scaler("MinMaxScaler")
        self.split_training_testing_sets()
        self.initiate_regressor()
        self.fit_gradient_boosting_model()
        self.generate_y_pred()

    def plot_feature_importances(self, feature_names):
        # set the feature names on XGBoost Booster
        self.model.get_booster().feature_names = feature_names
        # plot features ordered by their importance
        xgb.plot_importance(self.model)
        pyplot.show()

    def get_dmatrix(self):
        """
        get Data Matrix used in XGBoost
        :return:
        """
        self.data_dmatrix = xgb.DMatrix(data=self.x, label=self.y)

    def get_mae(self):
        """
        get mean_absolute_error from prediction
        :return:
        """
        return mean_absolute_error(self.y_test, self.y_pred)

    def get_rmse(self):
        """
        get mean_absolute_error from prediction
        :return:
        """
        return np.sqrt(mean_squared_error(self.y_test, self.y_pred))

    def get_k_fold_cross_validation(self):
        """
        Perform k-fold cross validation
        :return:
        """
        self.get_dmatrix()
        cv_results = xgb.cv(dtrain=self.data_dmatrix, params=self.params, nfold=3,
                            num_boost_round=50, early_stopping_rounds=10, metrics="rmse", as_pandas=True, seed=123)
        print(cv_results.head())
        print((cv_results["test-rmse-mean"]).tail(1))

    def plot_boosting_trees(self):
        xg_reg = xgb.train(params=self.params, dtrain=self.data_dmatrix, num_boost_round=10)
        # xgb.plot_tree(xg_reg, num_trees=0)
        # pyplot.rcParams['figure.figsize'] = [50, 10]
        # pyplot.show()

        xgb.plot_importance(xg_reg)
        pyplot.rcParams['figure.figsize'] = [5, 5]
        pyplot.show()