import unittest
import os


from src.red_wine_quality_modeling import RedWineQualityModeling


def get_resource_path(filename):
    return os.path.join(os.path.dirname(os.path.realpath(__file__)), "data", filename)


class TestRedWineQualityModeling(unittest.TestCase):

    @classmethod
    def setUpClass(cls) -> None:
        filename = "winequality-red.csv"
        data_file = get_resource_path(filename)
        cls.mymodel = RedWineQualityModeling(data_file=data_file)
        cls.mymodel.load_dataset()

    def test_LoadDataset_GivenWineQualityFile_ShouldReturnShapeTuple(self):
        self.assertTupleEqual((1599, 12), self.mymodel.df.shape)

    def test_SplitDfIntoXAndY_GivenDf_ShouldReturn2dataset(self):
        self.mymodel.split_df_into_x_and_y(column_name='quality')
        self.assertTupleEqual((1599, 11), self.mymodel.x.shape)
        self.assertTupleEqual((1599,), self.mymodel.y.shape)

    def test_XGBoostRregressor(self):
        self.mymodel.setup_regression_workflow()
        mae = self.mymodel.get_mae()
        rmse = self.mymodel.get_rmse()

    def test_KFoldCrossValidation(self):
        self.mymodel.setup_regression_workflow()
        self.mymodel.get_k_fold_cross_validation()

    def test_(self):
        self.mymodel.setup_regression_workflow()
        self.mymodel.get_k_fold_cross_validation()
        self.mymodel.plot_boosting_trees()
